# Openshift-acme deployment overview

[Let's Encrypt](https://letsencrypt.org) requires applications to be accessible from Internet to validate DNS domain ownership.
This openshift-acme is only functional in the prod Openshift environment as it's the only one that can be accessed from Internet.
The prod environment should thus use the Let's Encrypt "live" environment, while other clusters (not reachable from Internet)
are configured with the ["staging" environment](https://letsencrypt.org/docs/staging-environment/) and are not functional.

A Let's Encrypt account is created automatically by openshift-acme.

Because we need Acme validation routes to be Internet-visible so Let's Encrypt servers can validate domain ownership,
and routes are only visible to Intranet by default, we need
[a specific OPA rule](https://gitlab.cern.ch/paas-tools/infrastructure/openpolicyagent/merge_requests/6)
that override the default behavior and makes openshift-acme's validation routes visible to Internet.

We use the full SH256 to try to protect ourselves from any possible hijacking of the upstream
images hostes on quay.io.

## Updating controller and exposer images

Go to https://quay.io/repository/tnozicka/openshift-acme?tag=latest&tab=tags then, download to your local machine with `docker pull`
both `controller` and `exposer` images, like `docker pull quay.io/tnozicka/openshift-acme:exposer-0.9`.

After we have to tag the two new images with the command `docker tag`. E.g `docker tag quay.io/tnozicka/openshift-acme:exposer-0.9 registry.cern.ch/paas-tools/openshift-acme:controller-RELEASE.YEAR.MONTH.DAYTHOUR-MINUTES-SECONDSZ` (`controller-RELEASE.2021.06.08T11-00-00Z`).

Next we have to push them to Harbor with `docker push`. E.g `docker push registry.cern.ch/paas-tools/openshift-acme:controller-RELEASE.2021.06.08T11-00-00Z`.

Finally, we have to update the values on the `values.yaml` of the `openshift-acme` helm chart and update the targetRevision on okd4-install.
